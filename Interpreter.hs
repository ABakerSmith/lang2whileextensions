module Interpreter where

import Text.Megaparsec hiding (State)
import Data.Maybe
import ProcParser
import ArraySemantics

interpret :: String -> State
interpret ts = evalS parsedStm emptyState where
    parsedStm = fromJust (parseMaybe stm ts)

interpretFile :: String -> IO State
interpretFile fileName = do
    file <- readFile fileName
    return (interpret file)

printVar :: IO State -> Identifier -> IO ()
printVar x var = do
    s <- x
    putStrLn (show (s var))

printInterpret :: String -> Identifier -> IO ()
printInterpret = printVar . interpretFile
