module ArraySemantics where

import ProcParser

--------------------------------------------------------------------------------
-- State
--      Variables can be mapped to either arrays or single varibles. To
--      accomodate for this, both arrays and variables are mapped to arrays.
--      The arrays to which variables map only contain one element.
--------------------------------------------------------------------------------

type State = Identifier -> [Integer]

emptyState :: State
emptyState = \_ -> []

-- Updates value of `s x` to be `r`, all other values are unchanged.
update :: (Eq a) => a -> b -> (a -> b) -> (a -> b)
update x r f x' = if x' == x then r else f x'

--------------------------------------------------------------------------------
-- Arithmetic Expressions
--------------------------------------------------------------------------------

-- The semantic function for arithmetic expressions.
aVal :: Aexp -> State -> Integer
aVal (Literal n)        s = n
aVal (VarGet var)       s = head (s var)
aVal (ArrGet arr index) s = (s arr) !! fromInteger (aVal index s)
aVal (Mult x y)         s = (aVal x s) * (aVal y s)
aVal (Add x y)          s = (aVal x s) + (aVal y s)
aVal (Sub x y)          s = (aVal x s) - (aVal y s)

--------------------------------------------------------------------------------
-- Boolean Expressions
--------------------------------------------------------------------------------

-- The semantic function for boolean expressions.
bVal :: Bexp -> State -> Bool
bVal (TRUE)    s = True
bVal (FALSE)   s = False
bVal (Eq x y)  s = (aVal x s) == (aVal y s)
bVal (Le x y)  s = (aVal x s) <= (aVal y s)
bVal (Neg b)   s = not (bVal b s)
bVal (And x y) s = (bVal x s) && (bVal y s)

--------------------------------------------------------------------------------
-- Statements
--------------------------------------------------------------------------------

fix :: (a -> a) -> a
fix f = f (fix f)

-- A helper function that returns `x s` if `predicate s` evaulates to true, or
-- `y s` if `predicate s` evaulates to false.
cond :: (State -> Bool) -> (State -> State) -> (State -> State) -> (State -> State)
cond predicate x y s = if predicate s then x s else y s

-- The semantic function for statements.
evalS :: Stm -> State -> State
evalS (Skip)                        s = s
evalS (AssVar x aexp)               s = update x [(aVal aexp s)] s
evalS (AssArr x arr)                s = update x (map ((flip aVal) s) arr) s
evalS (Comp stm1 stm2)              s = (evalS stm2 . evalS stm1) s
evalS (If bexp stm1 stm2)           s = cond (bVal bexp) (evalS stm1) (evalS stm2) s
evalS (While bexp stm)              s = (fix f) s where
    f g = cond (bVal bexp) (g . (evalS stm)) id
evalS (Repeat stm bexp)             s = ((fix f) . evalS stm) s where
    f g = cond (bVal (Neg bexp)) (g . (evalS stm)) id
evalS (For initalise bexp iter stm) s = ((fix f) . evalS initalise) s where
    f g = cond (bVal bexp) (g . (evalS iter) . (evalS stm)) id
