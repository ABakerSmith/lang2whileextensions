module ProcParser where

import Control.Monad (void)
import Text.Megaparsec
import Text.Megaparsec.String
import Text.Megaparsec.Expr
import qualified Text.Megaparsec.Lexer as L
import Prelude hiding (Num)

--------------------------------------------------------------------------------
-- Abstract Grammar
--------------------------------------------------------------------------------
-- The abstract grammar for extended While language is defined in EBNF as
-- follows:
--
-- Numerals:
--      Num ::= Digit+
--
-- Variables:
--      VarName ::= Alpha AlphaNum*
--
-- Array Names:
--      ArrName ::= Alpha AlphaNum*
--
-- Arithmetic Expressions:
--      Aexp ::= Num
--             | VarName
--             | ArrName '[' Aexp ']'
--             | Aexp '+' Aexp
--             | Aexp '*' Aexp
--             | Aexp '-' Aexp
--
-- Boolean Expressions:
--      Bexp ::= 'true'
--             | 'false'
--             | Aexp '=' Aexp
--             | Aexp '<=' Aexp
--             | '!' Bexp
--             | Bexp '&' Bexp
--
-- Array Declarations:
--      ArrDec ::= '[' ']'
--               | '[' Aexp (, Aexp)* ']'
--
-- Statements:
--      Stm ::= 'skip'
--            | VarName ':=' Aexp
--            | ArrName ':=' ArrDec
--            | Stm ';' Stm
--            | 'if' Bexp 'then' Stm 'else' Stm
--            | 'while' Bexp 'do' Stm
--            | 'repeat' Stm 'until' Bexp
--            | 'for' Stm; Bexp; Stm do Stm

--------------------------------------------------------------------------------
-- Helper Functions
--------------------------------------------------------------------------------

-- Consumes a whole line comment.
whitespace :: Parser ()
whitespace = L.space (void spaceChar) lineCmnt blockCmnt
  where lineCmnt  = L.skipLineComment "//"
        blockCmnt = L.skipBlockComment "/*" "*/"

-- A note on the implementation above, `void` has the type signature
-- `void :: Functor f => f a -> f ()` and is used to ignore any spaces.

-- Succeeds if the parsed string is equal to `s`, followed by any whitespace.
tok :: String -> Parser String
tok t = string t <* whitespace

-- Parses a string enclosed in brackets.
parens :: Parser a -> Parser a
parens = between (tok "(") (tok ")")

--------------------------------------------------------------------------------
-- Numerals
--      Digit ::= '0' | .. | '9'
--      Num   ::= Digit+
--------------------------------------------------------------------------------

type Num = Integer

num :: Parser Num
num = do
    n <- some digitChar
    (return (read n)) <* space

--------------------------------------------------------------------------------
-- Identifier
--      Identifier ::= Alpha AlphaNum*
--------------------------------------------------------------------------------

-- A list of reserved keywords, these cannot be used
reserved :: [String]
reserved = ["true", "false", "skip", "if", "then", "else", "while", "do"]

type Identifier = String

-- Parsers identifiers, failing if the identifier is a reserved word.
identifier :: Parser Identifier
identifier = (str >>= check) <* space where
    str        = (:) <$> letterChar <*> many alphaNumChar
    check word = if word `elem` reserved
                    then fail $ "keyword " ++ show word ++ " cannot be an identifier"
                    else return word

--------------------------------------------------------------------------------
-- Variables:
--      VarName ::= Identifier
--------------------------------------------------------------------------------

varName :: Parser Identifier
varName = identifier

--------------------------------------------------------------------------------
-- Array Names:
--      ArrName ::= Identifier
--------------------------------------------------------------------------------

arrName :: Parser Identifier
arrName = identifier

--------------------------------------------------------------------------------
-- Arithmetic Expressions:
--      Aexp ::= Num
--             | VarName
--             | ArrName '[' Aexp ']'
--             | Aexp '+' Aexp
--             | Aexp '*' Aexp
--             | Aexp '-' Aexp
--------------------------------------------------------------------------------

data Aexp = Literal Num
          | VarGet Identifier
          | ArrGet Identifier Aexp
          | Mult Aexp Aexp
          | Add Aexp Aexp
          | Sub Aexp Aexp
          deriving (Eq, Show)

aexp' :: Parser Aexp
aexp' = parens aexp
       <|> Literal <$> num
       <|> try (ArrGet <$> arrName <* tok "[" <*> aexp <* tok "]")
       <|> VarGet <$> varName

aOperators :: [[Operator Parser Aexp]]
aOperators = [[InfixL (Mult <$ tok "*")],
              [InfixL (Add <$ tok "+"), InfixL (Sub <$ tok "-")]]

aexp :: Parser Aexp
aexp = makeExprParser aexp' aOperators

--------------------------------------------------------------------------------
-- Boolean Expressions:
--      Bexp ::= 'true'
--             | 'false'
--             | Aexp '=' Aexp
--             | Aexp '<=' Aexp
--             | '!' Bexp
--             | Bexp '&' Bexp
--------------------------------------------------------------------------------

data Bexp = TRUE
          | FALSE
          | Neg Bexp
          | And Bexp Bexp
          | Le Aexp Aexp
          | Eq Aexp Aexp
          deriving (Eq, Show)

bexp' :: Parser Bexp
bexp' = try (parens bexp)
    <|> TRUE  <$ tok "true"
    <|> FALSE <$ tok "false"
    <|> try (Le <$> aexp <* tok "<=" <*> aexp)
    <|> try (Eq <$> aexp <* tok "=" <*> aexp)

bOperators :: [[Operator Parser Bexp]]
bOperators = [[Prefix (Neg <$ tok "!")],
              [InfixL (And <$ tok "&")]]

bexp :: Parser Bexp
bexp = makeExprParser bexp' bOperators

--------------------------------------------------------------------------------
-- Array Declarations:
--      ArrDec ::= '[' ']'
--               | '[' Aexp (, Aexp)* ']'
--------------------------------------------------------------------------------

type ArrDec = [Aexp]

arrDec :: Parser ArrDec
arrDec = tok "[" *> sepBy aexp (tok ",") <* tok "]"

--------------------------------------------------------------------------------
-- Statements:
--      Stm ::= 'skip'
--            | VarName ':=' Aexp
--            | ArrName ':=' ArrDec
--            | Stm ';' Stm
--            | 'if' Bexp 'then' Stm 'else' Stm
--            | 'while' Bexp 'do' Stm
--            | 'repeat' Stm 'until' Bexp
--            | 'for' Stm; Bexp; Stm do Stm
--------------------------------------------------------------------------------

data Stm = Skip
         | AssVar Identifier Aexp
         | AssArr Identifier ArrDec
         | Comp Stm Stm
         | If Bexp Stm Stm
         | While Bexp Stm
         | Repeat Stm Bexp
         | For Stm Bexp Stm Stm
         deriving (Eq, Show)

stm' :: Parser Stm
stm' = try (parens stm)
   <|> try (AssVar <$> varName <* tok ":=" <*> aexp)
   <|> try (AssArr <$> arrName <* tok ":=" <*> arrDec)
   <|> Skip   <$ tok "skip"
   <|> If     <$ tok "if"    <*> bexp <*  tok "then" <*> stm <* tok "else" <*> stm
   <|> While  <$ tok "while" <*> bexp <*  tok "do"   <*> stm
   <|> Repeat <$ tok "repeat" <*> stm <* tok "until" <*> bexp
   <|> For    <$ tok "for" <*> stm <* tok "," <*> bexp <* tok "," <*> stm <* tok "do" <*> stm

-- Parses statements composed together with semicolons.
stmOperators :: [[Operator Parser Stm]]
stmOperators = [[InfixR (Comp <$ tok ";")]]

stm :: Parser Stm
stm = whitespace *> makeExprParser stm' stmOperators
